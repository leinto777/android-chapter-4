package com.example.myapplication.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.database.Keranjang
import com.example.myapplication.database.KeranjangDAO
import com.example.myapplication.database.KeranjangDatabase
import com.example.myapplication.database.KeranjangRepo
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class KeranjangViewModel(application: Application): ViewModel() {
    private val repo: KeranjangRepo = KeranjangRepo(application)

    val items: LiveData<List<Keranjang>> = repo.getAllCartItems()

    fun deleteItemCart(itemId: Int) {
        repo.deleteItemCart(itemId)
    }

    private fun updateQuantity(keranjang: Keranjang) {
        repo.updateQuantityItem(keranjang)
    }

    fun increment(keranjang: Keranjang) {
        val newTotal = keranjang.quantity + 1
        keranjang.quantity = newTotal
        keranjang.totalPrice = keranjang.basePrice?.times(newTotal)

        updateQuantity(keranjang)
    }

    fun decrement(keranjang: Keranjang) {
        val newTotal = keranjang.quantity - 1
        keranjang.quantity = newTotal
        keranjang.totalPrice = keranjang.basePrice?.times(newTotal)

        updateQuantity(keranjang)
    }
}