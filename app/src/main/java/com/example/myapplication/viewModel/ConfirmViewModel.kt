package com.example.myapplication.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import com.example.myapplication.database.Keranjang
import com.example.myapplication.database.KeranjangDAO
import com.example.myapplication.database.KeranjangDatabase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ConfirmViewModel(application: Application): ViewModel() {
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    private val keranjangDao: KeranjangDAO

    init {
        val db = KeranjangDatabase.getDataBase(application)
        keranjangDao = db.keranjangDAO()
    }

    fun deleteCart(keranjang: Keranjang){
        executorService.execute{
            keranjangDao.delete(keranjang)
        }
    }
}