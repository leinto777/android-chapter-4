package com.example.myapplication.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.database.Keranjang

class AdapterOrder: RecyclerView.Adapter<AdapterOrder.ViewHolderClass>() {

    private var items: List<Keranjang> = emptyList()

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(items: List<Keranjang>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderClass {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.order_view, parent, false)
        return ViewHolderClass(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderClass, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    inner class ViewHolderClass(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.tvKeranjangOrder)
        private val priceTextView: TextView = itemView.findViewById(R.id.tvPriceOrder)
        private val imageView: ImageView = itemView.findViewById(R.id.ivKeranjangOrder)
        private val qtyKeranjang: TextView = itemView.findViewById(R.id.totalOrderText)

        fun bind(item: Keranjang) {
            nameTextView.text = item.name
            priceTextView.text = item.basePrice.toString()
            qtyKeranjang.text = item.quantity.toString()
            imageView.setImageResource(item.images)
        }
    }
}