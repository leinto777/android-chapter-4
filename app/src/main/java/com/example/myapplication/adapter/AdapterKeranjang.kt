package com.example.myapplication.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.database.Keranjang
import com.example.myapplication.databinding.OrderViewBinding
import com.example.myapplication.viewModel.KeranjangViewModel
import com.google.android.material.snackbar.Snackbar

class AdapterKeranjang(private val keranjangViewModel: KeranjangViewModel): RecyclerView.Adapter<AdapterKeranjang.ViewHolderClass>() {

    private var items: List<Keranjang> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterKeranjang.ViewHolderClass {
        val binding = OrderViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolderClass(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderClass, position: Int) {
        val item = items[position]
        holder.bind(item, viewModel = keranjangViewModel)
    }

    class ViewHolderClass(private val binding: OrderViewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(keranjangItem: Keranjang, viewModel: KeranjangViewModel) {
            binding.ivKeranjangOrder.setImageResource(keranjangItem.images)
            binding.tvKeranjangOrder.text = keranjangItem.name
            binding.tvPriceOrder.text = keranjangItem.basePrice.toString()
            binding.layoutButton.totalOrderText.text = keranjangItem.quantity.toString()
            binding.viewNoteKeranjang.text= keranjangItem.note.toString()

            binding.deleteButton.setOnClickListener{
                viewModel.deleteItemCart(keranjangItem.id)
            }

            binding.layoutButton.plusButton.setOnClickListener {
                viewModel.increment(keranjangItem)
                binding.layoutButton.totalOrderText.text = keranjangItem.quantity.toString()
            }

            binding.layoutButton.minusButton.setOnClickListener {
                viewModel.decrement(keranjangItem)
                binding.layoutButton.totalOrderText.text = keranjangItem.quantity.toString()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(keranjangItem: List<Keranjang>) {
        this.items = keranjangItem
        notifyDataSetChanged()
    }

}

