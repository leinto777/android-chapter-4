package com.example.myapplication

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterFood(
    private val dataList: ArrayList<FoodClass>,
    private val gridMode: Boolean = true,
    private val onItemClick: ((FoodClass) -> Unit)? = null
): RecyclerView.Adapter<AdapterFood.ViewHolderClass>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterFood.ViewHolderClass {
        val layoutResId = if (gridMode) R.layout.grid_view else R.layout.list_view
        val view: View = LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
        return ViewHolderClass(view)
    }

    override fun onBindViewHolder(holder: ViewHolderClass, position: Int) {
        val currentItem = dataList[position]

        holder.rvImage.setImageResource(currentItem.image)
        holder.rvTitle.text = currentItem.name
        holder.rvSubTitle.text = currentItem.price.toString()

        holder.itemView.setOnClickListener{
            onItemClick?.invoke(currentItem)
            Log.d("ItemClicked", "Item clicked {$currentItem}")
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolderClass(itemView: View): RecyclerView.ViewHolder(itemView) {
        val rvImage: ImageView = itemView.findViewById(R.id.image)
        val rvTitle: TextView = itemView.findViewById(R.id.rvTitleText)
        val rvSubTitle: TextView = itemView.findViewById(R.id.rvSubTitleText)
    }

}

