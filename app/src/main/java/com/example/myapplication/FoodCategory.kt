package com.example.myapplication

data class FoodCategory(val name: String, val images: Int)