package com.example.myapplication.database

import android.app.Application
import androidx.lifecycle.LiveData
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class KeranjangRepo(application: Application) {
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()
    private val _keranjangDao: KeranjangDAO = KeranjangDatabase.getDataBase(application).keranjangDAO()


    init {
        _keranjangDao
    }

    fun insert(keranjang: Keranjang) {
        executorService.execute { _keranjangDao.insert(keranjang) }
    }

    fun getAllCartItems(): LiveData<List<Keranjang>> = _keranjangDao.getAllItems()

    fun deleteItemCart(itemId: Int) {
        executorService.execute {
            _keranjangDao.deleteItemById(itemId)
        }
    }

    fun updateQuantityItem(keranjang: Keranjang) {
        executorService.execute { _keranjangDao.update(keranjang) }
    }

}