package com.example.myapplication.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Keranjang::class], version = 1) // diganti saat manipulasi table
abstract class KeranjangDatabase: RoomDatabase() {

    abstract fun keranjangDAO(): KeranjangDAO

    companion object {
        private var INSTANCE: KeranjangDatabase? = null

        @JvmStatic
        fun getDataBase(context: Context): KeranjangDatabase {
            if (INSTANCE == null) {
                synchronized(KeranjangDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        KeranjangDatabase::class.java, "cart_database"
                    )
                        .build()
                }
            }
            return INSTANCE as KeranjangDatabase
        }
    }

}