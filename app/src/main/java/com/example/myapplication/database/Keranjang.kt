package com.example.myapplication.database

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "cart") //init tb name
data class Keranjang(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var name: String,
    var note: String?,
    var basePrice: Int? = 0,
    var totalPrice: Int? = 0,
    var quantity: Int = 1,
    var images: Int,
): Parcelable
