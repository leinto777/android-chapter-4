package com.example.myapplication.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface KeranjangDAO {
    @Insert
    fun insert(data: Keranjang)

    @Delete
    fun delete(data: Keranjang)

    @Update
    fun update(data: Keranjang)

    @Query("SELECT * FROM cart")
    fun getAllItems(): LiveData<List<Keranjang>>

    @Query("SELECT SUM(totalPrice) FROM cart")
    fun getTotalPrice(): LiveData<Int>

    @Query("DELETE FROM cart WHERE id = :itemId")
    fun deleteItemById(itemId: Int): Int
}