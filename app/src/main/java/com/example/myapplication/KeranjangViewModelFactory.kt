package com.example.myapplication

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.database.KeranjangDAO
import com.example.myapplication.viewModel.KeranjangViewModel

//class KeranjangViewModelFactory(private val keranjangDAO: KeranjangDAO) : ViewModelProvider.Factory {
//    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(KeranjangViewModel::class.java)) {
//            @Suppress("UNCHECKED_CAST")
//            return KeranjangViewModel(keranjangDAO) as T
//        }
//        throw IllegalArgumentException("Unknown ViewModel class")
//    }
//}