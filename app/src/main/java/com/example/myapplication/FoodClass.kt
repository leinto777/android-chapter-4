package com.example.myapplication

data class FoodClass(
    val image: Int,
    val name: String,
    val price: Int,
    val dataDescription: String?
)
