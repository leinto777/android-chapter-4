package com.example.myapplication

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.database.KeranjangDAO
import com.example.myapplication.viewModel.ConfirmViewModel

class ConfirmViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ConfirmViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ConfirmViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}