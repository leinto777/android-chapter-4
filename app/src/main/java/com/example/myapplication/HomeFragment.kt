package com.example.myapplication

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputBinding
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.databinding.FragmentHomeBinding
import com.example.myapplication.helper.SharedPreferenceHelper
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val categoryFoodList = ArrayList<FoodCategory>()
    private val foodData = ArrayList<FoodClass>()

    private var isListView = SharedPreferenceHelper.read("isListView", true)
    private var layoutType = SharedPreferenceHelper.read("layoutType", true)

    private val layoutIcon = arrayListOf(
        R.drawable.baseline_format_list_bulleted_24,
        R.drawable.baseline_grid_view_24
    )


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        //Show RV Category
        binding.rvCategory.setHasFixedSize(true)
        categoryFoodList.addAll(getFoodCategoryData())
        showRecyclerCategory()

        //Show RV Food
        binding.rvFood.setHasFixedSize(true)
        foodData.addAll(getFood())
        showRecyclerViewFood()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isListView = SharedPreferenceHelper.read("isListView", true)
        layoutType = SharedPreferenceHelper.read("layoutType", true)

        val toggleButton = binding.imageButton

        toggleButton.setOnClickListener{
            isListView = !isListView
            toggleRecyclerViewLayout()
            toggleImageViewImage(toggleButton)
        }

        toggleRecyclerViewLayout()

    }

    // Category
    @SuppressLint("Recycle")
    private fun getFoodCategoryData(): ArrayList<FoodCategory> {
        val catName = resources.getStringArray(R.array.categoryName)
        val catImg = resources.obtainTypedArray(R.array.categoryImages)

        val listCategory = ArrayList<FoodCategory>()
        for (i in catName.indices) {
            val food = FoodCategory(catName[i], catImg.getResourceId(i, -1))
            listCategory.add(food)
        }
        return listCategory
    }

    private fun showRecyclerCategory() {
        binding.rvCategory.layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        val listFoodAdapter = AdapterCategory(categoryFoodList)
        binding.rvCategory.adapter = listFoodAdapter
    }

    //Food
    @SuppressLint("Recycle")
    private fun getFood(): ArrayList<FoodClass> {
        val dataImg = resources.obtainTypedArray(R.array.foodImages)
        val dataName = resources.getStringArray(R.array.foodName)
        val dataPrice = resources.getIntArray(R.array.foodPrice)

        val listFood = ArrayList<FoodClass>()
        for (i in dataName.indices) {
            val food = FoodClass(dataImg.getResourceId(i, -1), dataName[i], dataPrice[i], null)
            listFood.add(food)
        }
        return listFood
    }

    private fun showRecyclerViewFood() {
        binding.rvFood.layoutManager =
            GridLayoutManager(requireActivity(), 2)
        val foodAdapter = AdapterFood(foodData)
        binding.rvFood.adapter = foodAdapter
    }

    // Toggle Button
    private fun toggleImageViewImage(imageView: ImageView) {
        imageView.setImageResource(layoutIcon[if (isListView) 0 else 1])
    }

    private fun showGridMenu() {
        binding.rvFood.layoutManager = GridLayoutManager(requireActivity(), 2)
        val adapterFood = AdapterFood(foodData, gridMode = true)
        binding.rvFood.adapter = adapterFood
    }
    private fun showLinearMenu() {
        binding.rvFood.layoutManager = LinearLayoutManager(requireActivity())
        val adapterFood = AdapterFood(foodData, gridMode = false)
        binding.rvFood.adapter = adapterFood
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun toggleRecyclerViewLayout() {
        foodData.clear()

        SharedPreferenceHelper.write("isListView", false)
        SharedPreferenceHelper.write("layoutType", false)

        layoutType = if (isListView) {
            showGridMenu()
            true
        } else {
            showLinearMenu()
            false
        }

        SharedPreferenceHelper.write("isListView", isListView)
        SharedPreferenceHelper.write("layoutType", layoutType)

        val adapter = AdapterFood(foodData, gridMode = isListView, onItemClick = { selectedItem ->

            val actionToDetailFragment = HomeFragmentDirections.actionHomeFragmentToDetailMenuFragment()
            actionToDetailFragment.nameDetail = selectedItem.name
            actionToDetailFragment.priceDetail = selectedItem.price.toString()
            actionToDetailFragment.imageDetail = selectedItem.image

            findNavController().navigate(actionToDetailFragment)
        })

        binding.rvFood.adapter = adapter

        foodData.addAll(getFood())
    }

}