package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapter.AdapterKeranjang
import com.example.myapplication.adapter.AdapterOrder
import com.example.myapplication.database.KeranjangDAO
import com.example.myapplication.database.KeranjangDatabase
import com.example.myapplication.databinding.FragmentConfirmOrderBinding
import com.example.myapplication.databinding.FragmentKeranjangBinding
import com.example.myapplication.viewModel.ConfirmViewModel
import com.example.myapplication.viewModel.KeranjangViewModel

class ConfirmOrderFragment : Fragment() {
    private var _binding: FragmentConfirmOrderBinding? = null
    private val binding get() = _binding!!

    private lateinit var keranjangViewModel: KeranjangViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentConfirmOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpCartViewModel()
        showRecyclerView()
        summary()
    }

    private fun setUpCartViewModel() {
        val viewModelFactory = ViewModelFactory(requireActivity().application)
        keranjangViewModel = ViewModelProvider(this, viewModelFactory)[KeranjangViewModel::class.java]
    }

    private fun showRecyclerView() {
        val adapterKeranjang = AdapterKeranjang(keranjangViewModel)

        binding.rvConfirmOrder.adapter = adapterKeranjang
        binding.rvConfirmOrder.layoutManager = LinearLayoutManager(requireContext())

        keranjangViewModel.items.observe(viewLifecycleOwner) {
            adapterKeranjang.setData(it)

            var totalPrice = 0
            it.forEach { item ->
                totalPrice += item.totalPrice!!
            }
        }
    }

    private fun summary() {
        keranjangViewModel.items.observe(viewLifecycleOwner) {
            var listMenu = ""
            var priceMenu = ""
            var totalPrice = 0
            it.forEach { item ->
                listMenu += "${item.name} - ${item.quantity} x ${item.basePrice}\n"
                priceMenu += "Rp. ${item.totalPrice}\n"
                totalPrice += item.totalPrice!!
            }

            val totalText = "Rp. $totalPrice"
            binding.tvKonfirmasiHargaPesanan.nameText.text = listMenu
            binding.tvKonfirmasiHargaPesanan.quantityText.text = priceMenu
            binding.tvKonfirmasiHargaPesanan.priceText.text = totalText
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}