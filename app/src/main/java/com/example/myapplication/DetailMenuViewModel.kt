package com.example.myapplication

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.database.Keranjang
import com.example.myapplication.database.KeranjangDAO
import com.example.myapplication.database.KeranjangDatabase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class DetailMenuViewModel(application: Application): ViewModel() {
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    private val keranjangDao: KeranjangDAO

    // Quantitiy
    private val _counter = MutableLiveData(1)
    val counter: LiveData<Int> = _counter

    private val _note = MutableLiveData("")
    val note: LiveData<String> = _note

    private val _selectedItem = MutableLiveData<DetailMenuFragmentArgs>()

    init {
        val db = KeranjangDatabase.getDataBase(application)
        keranjangDao = db.keranjangDAO()
    }

    fun initSelectedItem(food: DetailMenuFragmentArgs) {
        _selectedItem.value = food
    }

    fun addNote() {
        _note.value = _counter.value.toString()
    }

    fun incrementCount() {
        _counter.value = (_counter.value ?: 1) + 1
    }

    fun decrementCount() {
        val currentValue = _counter.value ?: 1
        if (currentValue > 1) {
            _counter.value = currentValue - 1
        }
    }

    private fun insertItem(keranjang: Keranjang) {
        executorService.execute{
            keranjangDao.insert(keranjang)
        }
    }

    fun addData(note: String) {
        val selectedItem = _selectedItem.value
        selectedItem?.let {
            val itemKeranjang = Keranjang(
                name = it.nameDetail.toString(),
                note = note,
                basePrice = it.priceDetail?.toInt(),
                totalPrice = it.priceDetail?.toInt()?.times(counter.value!!.toInt()),
                quantity = counter.value!!.toInt(),
                images = it.imageDetail
            )
            insertItem(itemKeranjang)
        }
    }

    /**
     * TODO
     * Buat LiveData Jumlah Pesanan (DONE)
     * Buat Total Price (BasePrice * Quantity)
     */

}