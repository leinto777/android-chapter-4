package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapter.AdapterKeranjang
import com.example.myapplication.database.KeranjangDAO
import com.example.myapplication.database.KeranjangDatabase
import com.example.myapplication.databinding.FragmentKeranjangBinding
import com.example.myapplication.databinding.OrderButtonsBinding
import com.example.myapplication.viewModel.KeranjangViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class KeranjangFragment : Fragment() {
    private var _binding: FragmentKeranjangBinding? = null
    private val binding get() = _binding!!

    private lateinit var keranjangViewModel: KeranjangViewModel

    private lateinit var adapterKeranjang: AdapterKeranjang

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentKeranjangBinding.inflate(inflater, container, false)
        setUpKeranjangViewModel()

        adapterKeranjang = AdapterKeranjang(keranjangViewModel)
        binding.rvKeranjang.setHasFixedSize(true)
        binding.rvKeranjang.layoutManager = LinearLayoutManager(requireContext())
        binding.rvKeranjang.adapter = adapterKeranjang

        keranjangViewModel.items.observe(viewLifecycleOwner) {
            adapterKeranjang.setData(it)

            var totalPrice = 0
            it.forEach { item ->
                totalPrice += item.totalPrice!!
            }
            val priceText = "Rp. $totalPrice"
            binding.tvTotalKeranjang.text = priceText
        }

        toConfirmOrder()
        return binding.root
    }

    private fun setUpKeranjangViewModel() {
        val viewModelFactory = ViewModelFactory(requireActivity().application)
        keranjangViewModel = ViewModelProvider(this, viewModelFactory)[KeranjangViewModel::class.java]
    }

    private fun toConfirmOrder() {
        binding.pesanBtnKeranjang.setOnClickListener {
            findNavController().navigate(
                R.id.action_keranjangFragment_to_confirmOrderFragment
            )
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}