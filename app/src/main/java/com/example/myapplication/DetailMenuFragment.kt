package com.example.myapplication

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.databinding.FragmentDetailMenuBinding


class DetailMenuFragment : Fragment() {

    private var _binding: FragmentDetailMenuBinding? = null
    private val binding get() = _binding!!

    // MutableLiveData
    private lateinit var viewModel: DetailMenuViewModel

    private val locationUri: String = "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailMenuBinding.inflate(inflater, container, false)

        val viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory)[DetailMenuViewModel::class.java]

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getArgs()

        openMap()
        withViewModel()

        addToKeranjang()

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun withViewModel() {
        // Live Data
        val observer = Observer<Int>{
            binding.orderButtons.totalOrderText.text = it.toString()

        }

        viewModel.counter.observe(viewLifecycleOwner, observer)

        binding.orderButtons.plusButton.setOnClickListener {
            viewModel.incrementCount()
        }

        binding.orderButtons.minusButton.setOnClickListener {
            viewModel.decrementCount()
        }

    }

    private fun openMap() {
        binding.detailLocation.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(locationUri))
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), "Google Maps tidak terinstal.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getArgs() {
        val data = DetailMenuFragmentArgs.fromBundle(arguments as Bundle)

        binding.nameDetail.text = data.nameDetail
        binding.priceDetail.text = data.priceDetail
        binding.imageDetail.setImageResource(data.imageDetail)

        viewModel.initSelectedItem(data)
    }

    private fun addToKeranjang() {

        binding.buttonOrder.setOnClickListener {
            val note = binding.noteLayout.noteValue.text.toString()
            viewModel.addData(note)
            Toast.makeText(requireContext(), "Success Add Data", Toast.LENGTH_SHORT).show()
        }
    }


}